<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{



	// made relation with package.
	// relation between package_id in member and id in package.
    public function package(){
        return $this->belongsTo(Package::class,'package_id','id');
    }

    public function user(){
        return $this->belongsTo(User::class,'member_id','id');
    }

    public function payments(){
        return $this->hasMany(Payment::class)->orderByDesc('id');
    }

}
