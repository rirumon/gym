<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{


    public function member(){
        return $this->belongsTo(Member::class,'member_id','id');
    }
    public function package(){
        return $this->belongsTo(Package::class,'package_id','id');
    }
}
