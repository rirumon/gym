<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public  function index(){
        $users = User::all();
        return view('home.user.index',compact('users'));
    }

    public function create(){
        return view('home.user.create');
    }

    public function store(Request $request){
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required', 'string', 'min:8', 'unique:users'],
        ]);
        $user =new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = Hash::make($request->password);

        if ($user->save()) {
            return redirect()->back()->with('success', 'User Create Successfully');
        } else {
            return redirect()->back()->with('failed', 'There are Some Problem Try again ');
        }
    }

    public  function show($id){
        try{
            $user = User::where('id',$id)->first();
            return view('home.user.show',compact('user'));
        }catch(Exception $e){
            return redirect()->back()->with('failed', 'There are Some Problem Try again '.$e);
        }
    }

    public function edit($id)
    {
        try{
            $user = User::where('id',$id)->first();
            return view('home.user.edit',compact('user'));
        }catch(Exception $e){
            return redirect()->back()->with('failed', 'There are Some Problem Try again '.$e);
        }

    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'phone' => ['required', 'string', 'min:8'],
            'id' => 'required',
        ]);
        try {
            $user = User::where('id', $request->id)->update([
                'name' => $request->name,
                'email' =>$request->email,
                'phone' =>$request->phone,
            ]);
            if ($user) {
                return redirect()->back()->with('success', 'User Update Successfully');
            } else {
                return redirect()->back()->with('failed', 'There are Some Problem Try again ');
            }
        } catch (Exception $e) {
            return redirect()->back()->with('failed', 'There are Some Problem Try again ' . $e);
        }

    }


    public function destroy($id)
    {
        if (User::where('id', $id)->delete())  {
            return redirect()->back()->with('success', 'User Delete Successfully');
        } else {
            return redirect()->back()->with('failed', 'There are Some Problem Try again ');
        }

    }

    public function isAdmin($id){
        $user = User::findorfail($id);
        $user = User::where('id', $id)->update([
            'is_admin' => !$user->is_admin,
        ]);
        if($user){
            return \request()->json(!$user->is_admin);
        }
    }
}
