<?php

namespace App\Http\Controllers;

use App\Model\Package;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	// returning view and passing list of packages
    public function index()
    {
        $packages = Package::all(); // getting list of packages.
        return view('home.package.index',compact('packages'));
    }

    public function create()
    {
        return view('home.package.create');
    }


    public function store(Request $request)
    {
		//
        $request->validate([
            'price'=>'required',
            'name'=>'required'
        ]);
        $package = new Package();
        $package->name = $request->name;
        $package->price = $request->price;
        $package->description = $request->description;
		
		// 
        if($package->save()){
			// 
            return redirect()->back()->with('success','Package Created Successfully');
        }else{
            return redirect()->back()->with('error','There are Some Problem. Please try again');
        }
    }

    public function edit($id)
    {
        try{
            $package = Package::find($id);
            return view('home.package.edit',compact('package'));
        }catch (\Exception $exception){
            return redirect()->back()->with('error','There are Some Problem. Please try again');
        }
    }

    public function update(Request $request)
    {
        $request->validate([
            'price'=>'required',
            'name'=>'required'
        ]);
        try{
            $package = Package::where('id',$request->id)->update([
                'name'=>$request->name,
                'price'=>$request->price,
                'description'=>$request->description
            ]);
            if ($package) {
                return redirect()->back()->with('success', 'Package Update Successfully');
            } else {
                return redirect()->back()->with('failed', 'There are Some Problem Try again');
            }
        }catch (\Exception $exception){
            return redirect()->back()->with('error','There are Some Problem. Please try again');
        }
    }

    public function destroy($id)
    {
        try{
            if (Package::where('id',$id)->delete()) {
                return redirect()->back()->with('success', 'Package Delete Successfully');
            } else {
                return redirect()->back()->with('failed', 'There are Some Problem Try again');
            }
        }catch (\Exception $exception){
            return redirect()->back()->with('error','There are Some Problem. Please try again');
        }
    }
}
