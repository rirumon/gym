<?php

namespace App\Http\Controllers;

use App\Model\Member;
use App\Model\Package;
use App\Model\Payment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MemberController extends Controller
{
    //return the members list with relation package
    public function index()
    {

        if(Auth::user()->is_admin != true){

            $member = Member::with('package')->with('payments')
                ->with('user')->where('id',Auth::user()->member_id)->firstOrFail();
//            return $member;
            return view('home.member.profile',compact('member'));
        }else{
            $members = Member::with('package')->with('user')->orderByDesc('id')->get();
            return view('home.member.index',compact('members'));
        }



    }

    //return the view with packages
    public function create()
    {
        //this is for dropdown option
        $packages = Package::all();
        return view('home.member.create',compact('packages'));
    }
   //this function for store the new member data
    public function store(Request $request)
    {
        //this is the input validate
        $request->validate([
            'name'=>'required',
            'package_id'=>'required',
            'phone'=>['required','unique:members'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        //append the input data in member object
        $member = new Member();
        $member->name = $request->name;
        $member->email = $request->email;
        $member->phone = $request->phone;
        $member->address = $request->address;
        $member->gender = $request->gender;
        $member->package_id = $request->package_id;
        //this for save the image in server
        if($request->hasFile('image')){
			$image = $request->file('image');
			$imageName =$request->email.'.'.$image->getClientOriginalExtension();
			//image upload this path
			$image->move(public_path('uploads/member'),$imageName);
			$member->image = $imageName;
		}
        //save or failed return the back route with message
        if($member->save()){
            //create user
            $user = new User();
            $user->email = $member->email;
            $user->name = $member->name;
            $user->phone = $member->phone ?? 0123;
            $user->password = Hash::make($request->password);
            $user->member_id = $member->id;
            $user->save();
            return redirect()->back()->with('success','New Member Created Successfully');
        }else{
            return redirect()->back()->with('error','There are Some Problem. Please try again');
        }
    }

   //return the single member data with package relation for show details
    public function show($id)
    {
        //this try catch for handel the exception
        try{
            $member = Member::with('package')->with('user')->findOrFirst($id);
            return view('home.member.show',compact('member'));
        }catch (\Exception $exception){
            return redirect()->back()->with('error','There are Some Problem. Please try again');
        }
    }

   //return the single member data with package relation for edit
    public function edit($id)
    {
        //try catch use for handel the exception
        try{
            $member = Member::with('package')->with('user')->find($id);
            //send the packages for dropdown option
            $packages = Package::all();
            return view('home.member.edit',compact('member','packages'));
        }catch (\Exception $exception){
            return redirect()->back()->with('error','There are Some Problem. Please try again');
        }
    }
  //this for update the member
    public function update(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'package_id.required'=>'Package is required',
        ]);

        if($request->password != null && $request->password_confirmation != null){
          $request->validate([
              'password' => ['required', 'string', 'min:8', 'confirmed'],
          ]);
          $user = User::where('member_id',$request->id)->update([
              'password'=>Hash::make($request->password)
          ]);
        }
        $imageName = null;
        //have new image
        if($request->hasFile('newImage')){
            //delete old image
            try {
                $path = 'uploads/member/'.$request->image;
                if(file_exists($path)){
                    unlink($path);
                }
            }catch (\Exception $e){}
            //save the new image
            $image = $request->file('newImage');
            $imageName = $request->email.'.'.$image->getClientOriginalExtension();
            $image->move(public_path('uploads/member'),$imageName);
        }else{
            $imageName = $request->image;
        }
        //update the member
        $member = Member::where('id',$request->id)->update([
            'name'=>$request->name,
            'phone'=>$request->phone,
            'address'=>$request->address,
            'image'=>$imageName,
            'gender'=>$request->gender,
            'package_id'=>$request->package_id,
        ]);
        //show the message and goto back action
        if ($member) {
            //update the password

            return redirect()->back()->with('success', 'Member Update Successfully');
        } else {
            return redirect()->back()->with('failed', 'There are Some Problem Try again');
        }
    }

   //this function delete the member with payment data form Member or payment table
    //this delete is soft delete / trash
    public function destroy($id)
    {
        //handle the exception and show the message
        try{
            if (Member::where('id',$id)->delete()) {
                Payment::where('member_id',$id)->delete();
                return redirect()->back()->with('success', 'Member Delete Successfully');
            } else {
                return redirect()->back()->with('failed', 'There are Some Problem Try again');
            }
        }catch (\Exception $exception){
            return redirect()->back()->with('error','There are Some Problem. Please try again');
        }
    }
}
