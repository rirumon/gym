<?php

namespace App\Http\Controllers;

use App\Model\Member;
use App\Model\Package;
use App\Model\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{

//    public function __construct()
//    {
//        $this->middleware('auth');
//    }
    //return the view with data members or payments only the current month

    public function index(){
        $packages = Package::all();
        return view('home.home',compact('packages'));
    }

    public function beMember(Request $request){
        $packages = Package::all();
        $id = $request->id == null ? 0 : $request->id;

        return view('home.create',compact('packages','id'));
    }
}
