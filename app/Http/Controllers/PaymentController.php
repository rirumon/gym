<?php

namespace App\Http\Controllers;

use App\Model\Member;
use App\Model\Package;
use App\Model\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    //check the register user is login
    public function __construct()
    {
        $this->middleware('auth');
    }

    //return the view with payments list relation with package or member
    // there have need a member id for show member payment history
    public function index($id){
         //handel the exception
        try{
            $payments = Payment::with('package')->with('member')->where('member_id',$id)->orderByDesc('id')->get();

            return view('home.payment.index',compact('payments'));
        }catch (\Exception $exception){
            return redirect()->back()->with('error','There are Some Problem. Please try again');
        }
    }

    //return the view with members for dropdown option
    public function create(){
        if (Auth::user()->is_admin){
            $members = Member::all();
        }else{
            $members = Member::where('id',Auth::user()->member_id)->get();
        }

        return view('home.payment.create',compact('members'));

    }

    //store the new payment history
    public function store(Request $request)
    {
        //validate the input data
        $request->validate([
            'member_id.required'=>'Member is required',
            'total_paid'=>'required',
        ]);
        // add input data in payment object
        $payment = new Payment();
        $payment->description = $request->description;
        $payment->member_id = $request->member_id;
        $member = Member::find($request->member_id);
        $payment->package_id = $member->package_id;
        $payment->total_paid = $request->total_paid;
        $package = Package::find($payment->package_id);
        //todo::check the total Payment have problems
//        $payment->due = ($package->price - $request->total_paid);
//        if ($package->due <0){
//            return redirect()->back()->with('error','Payment Input Amount Calculation is Wrong');
//        }
        //save and return back or show the message
        if($payment->save()){

            return redirect()->back()->with('success','Payment History Created Successfully');
        }else{
            return redirect()->back()->with('error','There are Some Problem. Please try again');
        }
    }


    //return all payment history with relation package or member
    public function indexTow(){
        $payments = Payment::with('package')->with('member')->orderByDesc('id')->get();
        return view('home.payment.index',compact('payments'));
    }
}
