<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Model\Member;
use App\Model\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    //
    public function  __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        if(Auth::user()->is_admin !=true){
        return redirect()->route('members.index');
        }
        $start1 = Carbon::parse(date('Y-M-d'))
            ->startOfMonth()
            ->toDateTimeString();
        //initial the current month last date time
        $end1 = Carbon::parse(date('Y-M-d'))
            ->endOfMonth()
            ->toDateTimeString();
        //there are the relation with package and,
        // implement the condition query and fetch only 5 data  from member table
        $members = Member::with('package')->whereBetween('created_at',[$start1,$end1])->orderByDesc('id')->limit(5)->get();
        //there are the relation with member or package table and,
        // implement the condition query and fetch only 5 data from payment table
        $payments = Payment::with('member')->with('package')
            ->whereBetween('created_at',[$start1,$end1])->orderByDesc('id')->limit(5)->get();
        //return the view with data members or payments
//        return view('home.home',);
        return view('dashboard.admin',compact('members','payments'));
    }
}
