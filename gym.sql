-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 21, 2020 at 04:25 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gym`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('Male','Female','Other') COLLATE utf8mb4_unicode_ci NOT NULL,
  `package_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `email`, `phone`, `image`, `address`, `gender`, `package_id`, `created_at`, `updated_at`) VALUES
(1, 'Thaddeus Pena', 'qibenahol@mailinator.net', '+1 (539) 305-2949', 'qibenahol@mailinator.net.png', 'Id nihil vel et acc', 'Other', 4, '2020-03-21 10:10:32', '2020-03-21 10:10:32'),
(2, 'Robin Hawkins', 'qexomu@mailinator.com', '+1 (861) 989-9878', 'qexomu@mailinator.com.png', 'Nostrum fugit venia', 'Female', 4, '2020-03-21 10:10:49', '2020-03-21 10:10:49'),
(3, 'Lesley Gillespie', 'ciwytu@mailinator.com', '+1 (488) 759-8893', 'ciwytu@mailinator.com.png', 'Laboriosam accusant', 'Female', 2, '2020-03-21 10:11:08', '2020-03-21 10:11:08'),
(4, 'Malachi Briggs', 'vuviwah@mailinator.net', '+1 (565) 931-6585', 'vuviwah@mailinator.net.png', 'Porro iusto dolore e', 'Male', 3, '2020-03-21 10:11:37', '2020-03-21 10:11:37'),
(5, 'Kiayada Thomas', 'xuhymox@mailinator.net', '+1 (377) 417-9432', NULL, 'Quasi ut voluptate l', 'Female', 3, '2020-03-21 10:11:49', '2020-03-21 10:11:49'),
(6, 'Nehru Mccullough', 'ryselonala@mailinator.com', '+1 (239) 238-8572', 'ryselonala@mailinator.com.png', 'Dicta qui ut est am', 'Female', 4, '2020-03-21 10:11:58', '2020-03-21 10:11:58'),
(7, 'Wyoming Cleveland', 'jupihevi@mailinator.net', '+1 (824) 905-5432', 'jupihevi@mailinator.net.jpg', 'Libero pariatur Mag', 'Female', 4, '2020-03-21 10:12:11', '2020-03-21 10:12:11'),
(8, 'Catherine Burt', 'tahubof@mailinator.net', '+1 (153) 546-4305', 'tahubof@mailinator.net.jpg', 'Aliqua Consequatur', 'Other', 2, '2020-03-21 10:12:23', '2020-03-21 10:12:23'),
(9, 'Ian Flowers', 'jeni@mailinator.com', '+1 (561) 572-1158', 'jeni@mailinator.com.jpg', 'Maxime ullamco quia', 'Other', 2, '2020-03-21 10:12:34', '2020-03-21 10:12:34'),
(10, 'Arthur Acosta', 'kujem@mailinator.net', '+1 (951) 552-8562', 'kujem@mailinator.net.jpg', 'Reprehenderit corru', 'Female', 2, '2020-03-21 10:12:44', '2020-03-21 10:12:44'),
(11, 'Moana Cervantes', 'pibutez@mailinator.net', '+1 (605) 794-2659', 'pibutez@mailinator.net.jpg', 'Quis aute et archite', 'Other', 2, '2020-03-21 10:12:54', '2020-03-21 10:12:54'),
(12, 'Skyler Joseph', 'ryhihodod@mailinator.net', '+1 (216) 568-4267', 'ryhihodod@mailinator.net.jpg', 'Commodo id aut tempo', 'Female', 2, '2020-03-21 10:13:08', '2020-03-21 10:13:08'),
(13, 'Fletcher Bowers', 'lyryh@mailinator.net', '+1 (483) 939-9781', 'lyryh@mailinator.net.jpg', 'Aute ut neque aut mo', 'Female', 4, '2020-03-21 10:13:18', '2020-03-21 10:13:18'),
(14, 'Darius Myers', 'matiruruvo@mailinator.com', '+1 (953) 114-4669', 'matiruruvo@mailinator.com.jpg', 'Mollitia expedita re', 'Other', 4, '2020-03-21 10:13:30', '2020-03-21 10:13:30'),
(15, 'Rumon', 'rumon@mail.com', '1232323423', 'rumon@mail.com.jpg', 'Jani na', 'Male', 1, '2020-03-21 10:19:20', '2020-03-21 10:19:20');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2012_02_26_161732_create_packages_table', 1),
(2, '2013_02_26_170504_create_members_table', 1),
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2019_08_19_000000_create_failed_jobs_table', 1),
(6, '2020_02_29_001627_create_payments_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `price`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Pro', 85, 'Doloribus autem labo', '2020-03-21 09:55:24', '2020-03-21 09:55:24'),
(2, 'Pre', 88, 'Nostrum fugit recus', '2020-03-21 09:55:32', '2020-03-21 09:55:32'),
(3, 'Rosalyn Bishop', 704, 'Adipisicing repudian', '2020-03-21 09:56:13', '2020-03-21 09:56:13'),
(4, 'Maile Rush', 437, 'Labore ipsa est qu', '2020-03-21 09:56:22', '2020-03-21 09:56:22');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_paid` double NOT NULL,
  `due` double NOT NULL DEFAULT '0',
  `member_id` bigint(20) UNSIGNED NOT NULL,
  `package_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `description`, `total_paid`, `due`, `member_id`, `package_id`, `created_at`, `updated_at`) VALUES
(1, 'Et molestiae labore', 11, 0, 4, 3, '2020-03-21 10:13:57', '2020-03-21 10:13:57'),
(2, 'Officia dolorem erro', 99, 0, 12, 2, '2020-03-21 10:14:02', '2020-03-21 10:14:02'),
(3, 'A ipsa dolor et non', 56, 0, 2, 4, '2020-03-21 10:14:06', '2020-03-21 10:14:06'),
(4, 'Et ipsum odit in mo', 90, 0, 6, 4, '2020-03-21 10:14:09', '2020-03-21 10:14:09'),
(5, 'Sunt hic alias molli', 85, 0, 3, 2, '2020-03-21 10:14:12', '2020-03-21 10:14:12'),
(6, 'Assumenda saepe dign', 2, 0, 4, 3, '2020-03-21 10:14:17', '2020-03-21 10:14:17'),
(7, 'Nihil rem modi quia', 11, 0, 11, 2, '2020-03-21 10:14:21', '2020-03-21 10:14:21'),
(8, 'Cumque nulla expedit', 37, 0, 2, 4, '2020-03-21 10:14:25', '2020-03-21 10:14:25'),
(9, 'Id eligendi autem se', 66, 0, 6, 4, '2020-03-21 10:14:28', '2020-03-21 10:14:28'),
(10, 'Tempor mollit tenetu', 18, 0, 9, 2, '2020-03-21 10:14:31', '2020-03-21 10:14:31'),
(11, 'Quidem facere exerci', 39, 0, 10, 2, '2020-03-21 10:14:35', '2020-03-21 10:14:35'),
(12, 'Aliquam aliquid qui', 23, 0, 12, 2, '2020-03-21 10:14:38', '2020-03-21 10:14:38'),
(13, 'Nemo molestiae rerum', 28, 0, 2, 4, '2020-03-21 10:14:41', '2020-03-21 10:14:41'),
(14, 'Qui provident neces', 24, 0, 7, 4, '2020-03-21 10:14:46', '2020-03-21 10:14:46'),
(15, 'In cumque eos cupida', 39, 0, 3, 2, '2020-03-21 10:14:50', '2020-03-21 10:14:50'),
(16, 'Atque ea rerum rerum', 67, 0, 14, 4, '2020-03-21 10:14:53', '2020-03-21 10:14:53'),
(17, 'Nemo quo dolore sit', 91, 0, 8, 2, '2020-03-21 10:15:00', '2020-03-21 10:15:00'),
(18, 'Sed nulla est esse u', 10, 0, 6, 4, '2020-03-21 10:15:05', '2020-03-21 10:15:05'),
(19, 'Voluptatem suscipit', 89, 0, 5, 3, '2020-03-21 10:15:09', '2020-03-21 10:15:09'),
(20, 'Deserunt nostrud eve', 53, 0, 10, 2, '2020-03-21 10:15:14', '2020-03-21 10:15:14'),
(21, 'Architecto dolor Nam', 20, 0, 8, 2, '2020-03-21 10:15:18', '2020-03-21 10:15:18'),
(22, 'Reprehenderit et qui', 92, 0, 6, 4, '2020-03-21 10:15:21', '2020-03-21 10:15:21'),
(23, 'Vero numquam similiq', 7, 0, 7, 4, '2020-03-21 10:15:25', '2020-03-21 10:15:25'),
(24, 'Ut irure dolore omni', 4, 0, 10, 2, '2020-03-21 10:15:29', '2020-03-21 10:15:29'),
(25, 'Aperiam consequat A', 61, 0, 2, 4, '2020-03-21 10:15:35', '2020-03-21 10:15:35'),
(26, 'Quia eiusmod provide', 69, 0, 8, 2, '2020-03-21 10:15:38', '2020-03-21 10:15:38'),
(27, 'Voluptatum dolore vo', 13, 0, 7, 4, '2020-03-21 10:15:42', '2020-03-21 10:15:42'),
(28, 'Sint quaerat consect', 4, 0, 10, 2, '2020-03-21 10:15:46', '2020-03-21 10:15:46'),
(29, 'Libero tenetur volup', 66, 0, 3, 2, '2020-03-21 10:16:43', '2020-03-21 10:16:43'),
(30, 'Omnis iusto laborum', 26, 0, 15, 1, '2020-03-21 10:19:51', '2020-03-21 10:19:51'),
(31, 'Voluptas impedit do', 81, 0, 15, 1, '2020-03-21 10:19:55', '2020-03-21 10:19:55'),
(32, 'Minima at quasi nihi', 94, 0, 15, 1, '2020-03-21 10:20:00', '2020-03-21 10:20:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_id` bigint(20) UNSIGNED DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `member_id`, `email_verified_at`, `password`, `is_admin`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'admin', 'admin@mail.com', NULL, NULL, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 1, NULL, '2020-03-21 09:53:56', '2020-03-21 09:53:56'),
(4, 'Thaddeus Pena', 'qibenahol@mailinator.net', '+1 (539) 305-2949', 1, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:10:32', '2020-03-21 10:10:32'),
(5, 'Robin Hawkins', 'qexomu@mailinator.com', '+1 (861) 989-9878', 2, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:10:49', '2020-03-21 10:10:49'),
(6, 'Lesley Gillespie', 'ciwytu@mailinator.com', '+1 (488) 759-8893', 3, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:11:08', '2020-03-21 10:11:08'),
(7, 'Malachi Briggs', 'vuviwah@mailinator.net', '+1 (565) 931-6585', 4, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:11:37', '2020-03-21 10:11:37'),
(8, 'Kiayada Thomas', 'xuhymox@mailinator.net', '+1 (377) 417-9432', 5, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:11:49', '2020-03-21 10:11:49'),
(9, 'Nehru Mccullough', 'ryselonala@mailinator.com', '+1 (239) 238-8572', 6, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:11:58', '2020-03-21 10:11:58'),
(10, 'Wyoming Cleveland', 'jupihevi@mailinator.net', '+1 (824) 905-5432', 7, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:12:11', '2020-03-21 10:12:11'),
(11, 'Catherine Burt', 'tahubof@mailinator.net', '+1 (153) 546-4305', 8, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:12:23', '2020-03-21 10:12:23'),
(12, 'Ian Flowers', 'jeni@mailinator.com', '+1 (561) 572-1158', 9, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:12:34', '2020-03-21 10:12:34'),
(13, 'Arthur Acosta', 'kujem@mailinator.net', '+1 (951) 552-8562', 10, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:12:44', '2020-03-21 10:12:44'),
(14, 'Moana Cervantes', 'pibutez@mailinator.net', '+1 (605) 794-2659', 11, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:12:55', '2020-03-21 10:12:55'),
(15, 'Skyler Joseph', 'ryhihodod@mailinator.net', '+1 (216) 568-4267', 12, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:13:08', '2020-03-21 10:13:08'),
(16, 'Fletcher Bowers', 'lyryh@mailinator.net', '+1 (483) 939-9781', 13, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:13:18', '2020-03-21 10:13:18'),
(17, 'Darius Myers', 'matiruruvo@mailinator.com', '+1 (953) 114-4669', 14, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:13:30', '2020-03-21 10:13:30'),
(18, 'Rumon', 'rumon@mail.com', '1232323423', 15, NULL, '$2y$10$nEf/WbJCtN5aGA40M9LRJuBx5xYzcw6MVRB7Tt62LjCujyVP9y7p.', 0, NULL, '2020-03-21 10:19:20', '2020-03-21 10:19:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `members_package_id_foreign` (`package_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_member_id_foreign` (`member_id`),
  ADD KEY `payments_package_id_foreign` (`package_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_member_id_foreign` (`member_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `members_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `payments_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
