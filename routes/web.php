<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/home','/dashboard');

Auth::routes();

//home page
Route::get('/', 'HomeController@index')->name('home');

Route::get('/dashboard','Dashboard\DashboardController@index');
Route::get('/be/member','HomeController@beMember')->name('be.member');

//user
Route::get('user/destroy/{id}','UserController@destroy')->name('users.destroy');
Route::get('user/create','UserController@create')->name('users.create');
//Route::get('user/create','UserManager\UserController@create')->middleware('permissions:user-create');
Route::post('user/store','UserController@store')->name('users.store');
Route::get('user/edit/{id}','UserController@edit')->name('users.edit');
Route::post('user/update','UserController@update')->name('users.update');
Route::get('user/show/{id}','UserController@show')->name('users.show');
Route::get('user/index','UserController@index')->name('users.index');

//package
Route::get('package/index','PackageController@index')->name('packages.index');
//Route::get('package/create','PackageController@create')->name('packages.create');
Route::post('package/store','PackageController@store')->name('packages.store');
Route::get('package/edit/{id}','PackageController@edit')->name('packages.edit');
Route::post('package/update','PackageController@update')->name('packages.update');
Route::get('package/destroy/{id}','PackageController@destroy')->name('packages.destroy');

Route::get('member/index','MemberController@index')->name('members.index');
Route::get('member/create','MemberController@create')->name('members.create');
Route::post('member/store','MemberController@store')->name('members.store');
Route::get('member/edit/{id}','MemberController@edit')->name('members.edit');
Route::post('member/update','MemberController@update')->name('members.update');
Route::get('member/destroy/{id}','MemberController@destroy')->name('members.destroy');
Route::get('member/show/{id}','MemberController@show')->name('members.show');



//payment
Route::get('payment/index/{id}','PaymentController@index')->name('payments.index');
Route::get('payments/index','PaymentController@indexTow')->name('payments.indexTow');
Route::get('payment/create','PaymentController@create')->name('payments.create');
Route::post('payment/store','PaymentController@store')->name('payments.store');

Route::get('user/admin/{id}','UserController@isAdmin');
