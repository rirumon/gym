<div class="container-fluid">
    <!-- There are the all runtime error show-->
    @if (count($errors) > 0)
        <div class="alert alert-danger m-3">
           <p><strong>Whoops!</strong> There were some problems with your input.</p>
            <br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show  m-3" role="alert"
             id="gone">
            <strong>{{$message}}.</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if($mess =Session::get('failed'))
        <div class="alert alert-warning alert-dismissible fade show  m-3" role="alert"
             id="gone">
            <strong>{{$mess}}.</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
</div>
