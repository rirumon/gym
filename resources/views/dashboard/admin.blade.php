@extends('layouts.app')

@section('content')

    <main role="main" class="container">


        <div class="my-3 p-3 bg-white rounded shadow-sm">
            <h6 class="border-bottom border-gray pb-2 mb-0">Recent Members</h6>
            @foreach($members as $item)
                <div class="media text-muted pt-3">
                    @if($item->image !=null)
                        <img class="bd-placeholder-img mr-2 rounded" src="{{url('uploads/member',$item->image)}}" width="32" height="32" >
                    @else
                        <svg class="bd-placeholder-img mr-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 32x32"><title>Placeholder</title><rect width="100%" height="100%" fill="#007bff"/><text x="50%" y="50%" fill="#007bff" dy=".3em">32x32</text></svg>
                    @endif

                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">@ {{$item->name}}</strong>
                        Package : {{$item->package->name}}    Email : {{$item->email ?? 'N/A'}}       Phone : {{$item->phone ?? 'N/A'}}.
                    </p>
                </div>
            @endforeach

            <small class="d-block text-right mt-3">
                <a href="{{route('members.index')}}">All members</a>
            </small>
        </div>

        <div class="my-3 p-3 bg-white rounded shadow-sm">
            <h6 class="border-bottom border-gray pb-2 mb-0">Payment History</h6>
            @foreach($payments as $item)
                <div class="media text-muted pt-3">
                    @if($item->member->image !=null)
                        <img class="bd-placeholder-img mr-2 rounded" src="{{url('uploads/member',$item->member->image)}}" width="32" height="32" >
                    @else
                        <svg class="bd-placeholder-img mr-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 32x32"><title>Placeholder</title><rect width="100%" height="100%" fill="#007bff"/><text x="50%" y="50%" fill="#007bff" dy=".3em">32x32</text></svg>
                    @endif
                    <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <div class="d-flex justify-content-between align-items-center w-100">
                            <strong class="text-gray-dark">{{$item->member->name}}</strong>
                            <a href="{{route('payments.index',$item->member->id)}}">History</a>
                        </div>
                        <span class="d-block"><span class="text-primary">Paid {{$item->total_paid}}  </span>@ {{$item->description}}</span>
                    </div>
                </div>
            @endforeach
            <small class="d-block text-right mt-3">
                <a href="{{route('payments.indexTow')}}">All Payment</a>
            </small>
        </div>
    </main>
@endsection


