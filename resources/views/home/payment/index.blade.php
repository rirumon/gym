@extends('layouts.app')

@section('content')


    <div class="">
        <div class="card p-2 shadow-lg">
            <table class="table table-borderless">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Member</th>
                    <th scope="col">Package</th>
                    <th scope="col">Paid</th>
{{--                    <th scope="col">Due</th>--}}
                    <th scope="col">Description</th>
                    <th scope="col">Date</th>
                </tr>
                </thead>
                <tbody>
{{--                @php--}}
{{--                $total_paid = 0;--}}
{{--                $total_due = 0;--}}
{{--                @endphp--}}
                @foreach($payments as $item)
                    <tr>
                        <th scope="row">{{$loop->index+1}}</th>
                        <th>{{$item->member->name}}<br/>{{$item->member->email ?? $item->member->phone}}</th>
                        <th scope="row">{{$item->package->name}}({{$item->package->price}})</th>
                        <td class="text-primary">{{$item->total_paid}}
{{--                            <input type="hidden" value="{{$total_paid +=$item->total_paid }}"></td>--}}
{{--                        <td class="text-danger">{{$item->due}}--}}
{{--                        <input type="hidden" value="{{$total_due +=$item->due }}">--}}
{{--                        </td>--}}
                        <td>{{$item->description}}</td>
                        <td>{{date('d-M-Y',strtotime($item->created_at))}}<br>
                        </td>
                    </tr>
                @endforeach
{{--                <tr>--}}
{{--                    <th scope="row"></th>--}}
{{--                    <th></th>--}}
{{--                    <th scope="row"></th>--}}
{{--                    <td class="text-primary">Total: {{$total_paid}}</td>--}}
{{--                    <td class="text-danger">Total: {{$total_due}}</td>--}}
{{--                    <td></td>--}}
{{--                    <td><br>--}}
{{--                    </td>--}}
{{--                </tr>--}}
                </tbody>
            </table>
        </div>
    </div>

@endsection

