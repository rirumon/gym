@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <h2 class="card-header">Payment create</h2>
            <div class="card-body p-5">
                <form method="post" action="{{route('payments.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Member Select</label>
                        <select class="js-example-basic-single form-control custom-select" name="member_id" required>
                            @foreach($members as $item)
                                <option value="{{$item->id}}">{{$item->name}} ({{$item->email ?? $item->phone}})  </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Amount</label>
                        <input name="total_paid" min="0" class="form-control" type="number" required>
                    </div>

                    <div class="form-group">
                        <label>Description</label>
                        <input id="" name="description" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-outline-primary m-2">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
