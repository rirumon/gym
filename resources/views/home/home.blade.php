@extends('layouts.app')

@section('content')

    <main role="main" class="container">
        <div class="row">
            @foreach($packages as $item)
                <div class="col-4">
                    <div class="card m-2">
                        <div class="card-header">
                            <h3>{{$item->name}}, <strong class="text-success">${{$item->price}}</strong></h3>
                        </div>
                        <div class="card-body">
                            {!! $item->description !!}
                        </div>
                        <div class="card-footer">
                            <a href="{{ route('be.member', 'id='.$item->id) }}" class="btn btn-outline-secondary m-1">Select A Package</a>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>

    </main>
@endsection

