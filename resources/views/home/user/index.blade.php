@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->

<div class="kt-portlet kt-portlet--mobile">
    <div class="float-left p-2 ">
        <a onclick="forModal('{{ route('users.create') }}', 'Member Edit')" class="btn btn-primary mb-5">Add User</a>
    </div>

    <div class="kt-portlet__body">
        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php $a = 1 ?>
            @foreach($users as $item)
                <tr>
                    <td>{{$a++}}</td>
                    <td>Name : {{$item->name}} <br> Email : {{$item->email}}</td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('users.edit', $item->id) }}" >Edit</a>
                                <a class="dropdown-item" href="{{ route('users.show', $item->id) }}" >Show</a>
                                <a class="dropdown-item" href="#" onclick="confirm_modal('{{ route('users.destroy',$item->id) }}')">Delete</a>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach

            </tbody>
            <tfoot>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
        <!--end: Datatable -->
    </div>
</div>

@endsection
