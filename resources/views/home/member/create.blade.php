
        <form method="post" action="{{route('members.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Name</label>
                <input name="name" class="form-control" type="text" required>
            </div>

            <div class="form-group">
                <label>Email</label>
                <input name="email"  class="form-control" type="email">
            </div>

            <div class="form-group">
                <label>Phone</label>
                <input name="phone"  class="form-control" type="tel">
            </div>

            <div class="form-group">
                <label>Image</label>
                <input name="image" class="form-control-file" type="file">
            </div>
            <div class="form-group">
                <label>Select Gender</label>
                <select class="form-control custom-select" name="gender">
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                    <option value="Other">Other</option>
                </select>
            </div>

            <div class="form-group">
                <label>Select Package</label>
                <select class="form-control custom-select" name="package_id">
                    @foreach($packages as $item)
                        <option value="{{$item->id}}">{{$item->name}}({{$item->price}})</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label>Address</label>
				<textarea name="address" class="form-control"></textarea>
            </div>

            <div class="form-group">
                <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>

                <div class="">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                <div class="">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>
            </div>

            
            <button type="submit" class="btn btn-outline-primary m-2">Save</button>
        </form>
