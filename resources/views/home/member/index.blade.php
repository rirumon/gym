@extends('layouts.app')

@section('content')


    <div class="">


        <div class="card p-2 shadow-lg">
            @if(\Illuminate\Support\Facades\Auth::user()->is_admin == true)
            <div class="float-left p-2">
                <a onclick="forModal('{{ route('members.create') }}', 'Member Edit')" class="btn btn-primary">Add Member</a>
            </div>
            @endif
            <table class="table table-borderless">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Gender</th>
                    <th scope="col">Package</th>
                    @if(\Illuminate\Support\Facades\Auth::user()->is_admin == true)
                    <th scope="col">Admin</th>
                    @endif
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($members as $item)
                    <tr>
                        <th scope="row">{{$loop->index+1}}</th>
                        <td>{{$item->name}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->phone}}</td>
                        <td>{{$item->gender}}</td>
                        <td>{{$item->package->name}}</td>
                        @if(\Illuminate\Support\Facades\Auth::user()->is_admin == true)
                            <td><a href="#!" onclick="adminChange({{$item->id}})"><span id="is-admin">
                                        @if($item->user != null)
                                        {{$item->user->is_admin == true ? 'Admin' : 'Member'}}
                                            @else
                                        Member
                                        @endif
                                    </span></a></td>
                        @endif
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#" onclick="forModal('{{ route('members.edit', $item->id) }}', 'Member Update')">Edit</a>
                                    <a class="dropdown-item" href="#" onclick="confirm_modal('{{ route('members.destroy',$item->id) }}')">Delete</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{route('payments.index',$item->id)}}">Payment</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
