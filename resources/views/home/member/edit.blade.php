<form method="post" action="{{route('members.update')}}" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="id" value="{{$member->id}}">
    <div class="form-group">
        <label>Name</label>
        <input name="name" class="form-control" value="{{$member->name}}" type="text" required>
    </div>

    <div class="form-group">
        <label>Email</label>
        <input readonly class="form-control" value="{{$member->email}}" type="email">
    </div>

    <div class="form-group">
        <label>Phone</label>
        <input name="phone" class="form-control" value="{{$member->phone}}" type="tel">
    </div>

    <div class="form-group">
        <label>New Image</label>
        <input name="image" class="form-control-file" value="{{$member->image}}" type="hidden">
        <input name="newImage" class="form-control-file" type="file">
    </div>
    <img src="{{url('uploads/member',$member->image)}}" class="img-fluid" width="100" height="100">
    <div class="form-group">
        <label>Select Gender</label>
        <select class="form-control custom-select" name="gender">
            <option value="Male" {{$member->gender == "Male" ? 'selected' : null}}>Male</option>
            <option value="Female" {{$member->gender == "Female" ? 'selected' : null}}>Female</option>
            <option value="Other" {{$member->gender == "Other" ? 'selected' : null}}>Other</option>
        </select>
    </div>

    <div class="form-group">
        <label>Select Package</label>
        <select class="form-control custom-select" name="package_id">
            @foreach($packages as $item)
                <option value="{{$item->id}}" {{$item->id == $member->package->id ? 'selected': null}}>{{$item->name}}
                    ({{$item->price}})
                </option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Address</label>
        <input id="" name="address" value="{{$member->address}}" class="form-control">
    </div>
    @if(\Illuminate\Support\Facades\Auth::user()->is_admin != true)
        <div class="form-group">
            <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                       name="password"  autocomplete="current-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group">
            <label for="password-confirm" class="col-form-label text-md-right">{{ __('Confirm Password') }}</label>

            <div class="">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                       autocomplete="new-password">
            </div>
        </div>
    @endif
    <button type="submit" class="btn btn-outline-primary m-2">Save</button>
</form>
