@extends('layouts.app')

@section('content')
{{--    {{$member}}--}}
    <div class="container emp-profile">

            <div class="row">
                <div class="col-md-4">
                    @if($member->image != null)
                    <div class="profile-img">
                        <img src="{{url('uploads/member',$member->image)}}" alt="{{$member->name}}" style="height:160px;"/>
                    </div>
                    @endif
                </div>
                <div class="col-md-6">
                    <div class="profile-head">
                        <h5>
                            {{$member->name}}
                        </h5>
                        <h6>
                           {{$member->email}}
                        </h6>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Payments</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <button class="profile-edit-btn"  onclick="forModal('{{ route('members.edit', $member->id) }}', 'Member Edit Profile')">Edit Profile</button>

                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="profile-work">
                        <p>About</p>
                        <p>Phone : {{$member->phone ?? null}}</p>
                        <p>Address : {{$member->address ?? null}}</p>
                        <p>Package : {{$member->package->name}}</p>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="tab-content profile-tab" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
{{--                                <div class="col-md-6">--}}
{{--                                    <label>User Id</label>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-6">--}}
{{--                                    <p>Kshiti123</p>--}}
{{--                                </div>--}}
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{$member->name}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Email</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{$member->email}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Phone</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{$member->phone}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Package</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{$member->package->name}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Package</th>
                                    <th scope="col">Paid</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($member->payments as $item)
                                    <tr>
                                        <td scope="row">{{$loop->index+1}}</td>
                                        <td scope="row">{{$item->package->name}}({{$item->package->price}})</td>
                                        <td class="text-primary">{{$item->total_paid}}
                                        <td>{{$item->description}}</td>
                                        <td>{{date('d-M-Y',strtotime($item->created_at))}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

    </div>
    @endsection
