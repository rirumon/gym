

@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-6">
            <div class="card">
                <h2 class="card-header">Package create</h2>
                <div class="card-body p-5">
                    <form method="post" action="{{route('packages.store')}}">
                        @csrf
                        <div class="form-group">
                            <label>Name</label>
                            <input name="name" class="form-control" type="text" required>
                        </div>

                        <div class="form-group">
                            <label>Price</label>
                            <input name="price" class="form-control" type="number" required>
                        </div>

                        <div class="form-group">
                            <label>Description</label>
                            <input id="" name="description" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-outline-primary m-2">Save</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="row">
                @foreach($packages as $item)
                    <div class="col-6">
                        <div class="card ">
                            <div class="card-header">
                                <h3>{{$item->name}}, <strong class="text-success">${{$item->price}}</strong></h3>
                            </div>
                            <div class="card-body">
                                {!! $item->description !!}
                            </div>
                            <div class="card-footer">
                                <a onclick="forModal('{{ route('packages.edit', $item->id) }}', 'Package Edit')" class="btn btn-outline-secondary m-1">Edit</a>
                                <a onclick="confirm_modal('{{ route('packages.destroy',$item->id) }}')" href="#!" class="btn btn-outline-danger m-1">Delete</a>
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>

        </div>
    </div>

@endsection

