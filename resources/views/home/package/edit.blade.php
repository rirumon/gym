<form method="post" action="{{route('packages.update')}}">
    @csrf
    <input type="hidden" name="id" value="{{$package->id}}">
    <div class="form-group">
        <label>Name</label>
        <input name="name" value="{{$package->name}}" class="form-control" type="text" required>
    </div>

    <div class="form-group">
        <label>Price</label>
        <input name="price" value="{{$package->price}}" class="form-control" type="text" required>
    </div>

    <div class="form-group">
        <label>Description</label>
        <input class="form-control" name="description" value="{{$package->description}}">
    </div>
    <button type="submit" class="btn btn-outline-primary">Update</button>
</form>

